﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;

namespace TrainingMVC.Models.Training
{
    [Table("tbl_employee")]
    public class tbl_employee
    {
        [Key]
        public string nrp { get; set; }
        public string nama { get; set; }
        public string alamat { get; set; }
        public string password { get; set; }

        public async Task<bool> getValidUser(string Username, string Password)
        {
            Boolean Result = false;
            TrainingContext db_training = new TrainingContext();
            Result = db_training.tbl_employee.Where(f => f.nrp == Username && f.password == Password).Count() > 0 ? true : false;
            return Result;
        }

        public IQueryable<tbl_employee> getListEmployee()
        {
            TrainingContext db_training = new TrainingContext();
            var data = db_training.tbl_employee;
            return data;
        }

        public ReturnValue insertEmployee(tbl_employee stbl_employee)
        {
            TrainingContext db_training = new TrainingContext();
            ReturnValue returnVal = new ReturnValue();
            var dataexisting = db_training.tbl_employee
                                .Where(f => f.nrp == stbl_employee.nrp).FirstOrDefault();
            if (dataexisting == null)
            {
                tbl_employee itbl = new tbl_employee();

                itbl.nrp = stbl_employee.nrp;
                itbl.nama = stbl_employee.nama;
                itbl.alamat = stbl_employee.alamat;

                db_training.tbl_employee.Add(itbl);
                db_training.SaveChanges();
                db_training.Dispose();

                returnVal.status = true;
                returnVal.remarks = "Berhasil Insert";
                return returnVal;
            }
            else
            {
                returnVal.status = false;
                returnVal.remarks = "Gagal Insert, nrp " + stbl_employee.nrp + " sudah ada didalam database";
                return returnVal;

            }
        }

        public ReturnValue updateEmployee(tbl_employee sParam)
        {
            TrainingContext db_training = new TrainingContext();
            ReturnValue returnVal = new ReturnValue();
            var iTbl = db_training.tbl_employee
                        .Where(f => f.nrp == sParam.nrp).FirstOrDefault();

            if (iTbl != null)
            {
                iTbl.nama = sParam.nama;
                iTbl.alamat = sParam.alamat;

                db_training.tbl_employee.Update(iTbl);
                db_training.SaveChanges();
                db_training.Dispose();

                returnVal.status = true;
                returnVal.remarks = "Berhasil Update";
                return returnVal;
            }
            else
            {
                returnVal.status = false;
                returnVal.remarks = "Gagal Update, nrp " + sParam.nrp + " tidak ada didalam database";
                return returnVal;
            }
        }

        public ReturnValue deleteEmployee(string nrp)
        {
            TrainingContext db_training = new TrainingContext();
            ReturnValue returnVal = new ReturnValue();

            var iTbl = db_training.tbl_employee
                        .Where(f => f.nrp == nrp).FirstOrDefault();

            if (iTbl != null)
            {
                db_training.tbl_employee.Remove(iTbl);
                db_training.SaveChanges();
                db_training.Dispose();

                returnVal.status = true;
                returnVal.remarks = "Berhasil Delete";
                return returnVal;
            }
            else
            {
                returnVal.status = false;
                returnVal.remarks = "Gagal Delete, nrp " + nrp + " tidak ada didalam database";
                return returnVal;
            }
        }

        public ReturnValue insertListEmployee(List<tbl_employee> sParam)
        {
            TrainingContext db_training = new TrainingContext();
            ReturnValue returnVal = new ReturnValue();
            int count_fail = 0;
            int count_success = 0;

            foreach (var item in sParam)
            {
                var existingdata = db_training.tbl_employee
                                    .Where(f => f.nrp == item.nrp).FirstOrDefault();

                if (existingdata == null)
                {
                    tbl_employee iTbl = new tbl_employee();

                    iTbl.nrp = item.nrp;
                    iTbl.nama = item.nama;
                    iTbl.alamat = item.alamat;

                    db_training.tbl_employee.Add(iTbl);
                    db_training.SaveChanges();

                    count_success += 1;
                }
                else
                {
                    count_fail += 1;
                }
            }
            db_training.Dispose();

            returnVal.status = true;
            returnVal.remarks = "Berhasil Insert : " + count_success + " data & Gagal Insert : " + count_fail + " data";
            return returnVal;
        }
    }
}
