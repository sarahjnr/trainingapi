﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using Microsoft.AspNetCore.Authorization;
using TrainingMVC.Models;
using Microsoft.IdentityModel.Tokens;
using System.Text;
using System.Security.Claims;
using System.IdentityModel.Tokens.Jwt;

// For more information on enabling Web API for empty projects, visit https://go.microsoft.com/fwlink/?LinkID=397860

namespace TrainingMVC.Controllers
{
    [Route("api/[controller]")]
    public class TokenController : Controller
    {
        private IConfiguration _config;

        public TokenController(IConfiguration config)
        {
            _config = config;
        }

        [AllowAnonymous]
        [HttpPost]
        public async Task<JsonResult> CreateToken([FromBody]LoginModel login)
        {
            var user = await new tbl_employee().getValidUser(login.Username, login.Password);

            if (user)
            {
                var userdata = await Authenticate(login);
                var tokenString = BuildToken(userdata);
                var response = new
                {
                    token = string.Format("Bearer {0}", tokenString),
                    expired_date = DateTime.Now.AddDays(30).ToString("yyyy-MM-dd HH:mm:ss"),
                    expired = (int)60 * 60 * 24 * 30,
                    success = true
                };

                return Json(response);
            }

            var err_response = new
            {
                token = "Invalid user !",
                expired = 0,
                success = false,
                remarks = "Invalid user !"
            };

            return Json(err_response);
        }

        private string BuildToken(UserModel user)
        {
            var key = new SymmetricSecurityKey(Encoding.UTF8.GetBytes(_config["Jwt:Key"]));
            var creds = new SigningCredentials(key, SecurityAlgorithms.HmacSha256);
            var claims = new[]
                {
                    new Claim(JwtRegisteredClaimNames.NameId, user.Nrp),
                    new Claim(JwtRegisteredClaimNames.GivenName, user.Name),
                    new Claim(JwtRegisteredClaimNames.UniqueName, user.Nrp),
                    new Claim(JwtRegisteredClaimNames.Email, user.Nrp )
                };

            var token = new JwtSecurityToken(_config["Jwt:Issuer"],
              _config["Jwt:Issuer"],
              claims,
              expires: DateTime.Now.AddDays(30),
              signingCredentials: creds);

            return new JwtSecurityTokenHandler().WriteToken(token);
        }

        private async Task<UserModel> Authenticate(LoginModel login)
        {
            //login.Username = login.Username.Substring(1);
            using (var db_ = new TrainingContext())
            {
                var tbl = db_.tbl_employee.Where(f => f.nrp == login.Username).FirstOrDefault();

                if (tbl == null)
                    return new UserModel();

                var user = new UserModel
                {
                    Nrp = tbl.nrp,
                    Name = tbl.nama
                };

                return user;
            }
        }

        public class LoginModel
        {
            public string Username { get; set; }
            public string Password { get; set; }
            public string App { get; set; }
        }
    }
}
