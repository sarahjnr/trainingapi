﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;

namespace TrainingMVC.Controllers
{
    public class AccountController : Controller
    {
        [HttpGet]
        [Route("Account/Login")]
        public IActionResult Login()
        {
            return Json("Invalid-Token");
        }
    }
}
